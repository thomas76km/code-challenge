package com.mycomp.flight.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mycomp.flight.domain.Fltschmst;

@Repository
public interface FlightRepository extends JpaRepository<Fltschmst, Long> {
	
	Fltschmst findFlightByOrgArpAndDstArp (@Param("orgArp") String orgArp, @Param("dstArp") String dstArp);

}
