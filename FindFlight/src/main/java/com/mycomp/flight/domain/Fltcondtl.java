package com.mycomp.flight.domain;

import java.io.Serializable;
import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the fltcondtl database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Fltcondtl.findAll", query="SELECT f FROM Fltcondtl f"),
	@NamedQuery(name="Fltcondtl.findConByFltId", query="SELECT c FROM Fltcondtl c where c.fltId=:fltId")
})

public class Fltcondtl implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="FLTCONDTL_ID")
	private int fltcondtlId;

	@Column(name="CON_ARP")
	private String conArp;

	@Column(name="DEP_TIM")
	private Timestamp depTim;

	//bi-directional many-to-one association to Fltschmst
	@ManyToOne
	@JoinColumn(name="FLT_ID")
	private Fltschmst fltschmst;

	public Fltcondtl() {
	}

	public int getFltcondtlId() {
		return this.fltcondtlId;
	}

	public void setFltcondtlId(int fltcondtlId) {
		this.fltcondtlId = fltcondtlId;
	}

	public String getConArp() {
		return this.conArp;
	}

	public void setConArp(String conArp) {
		this.conArp = conArp;
	}

	public Timestamp getDepTim() {
		return this.depTim;
	}

	public void setDepTim(Timestamp depTim) {
		this.depTim = depTim;
	}

	public Fltschmst getFltschmst() {
		return this.fltschmst;
	}

	public void setFltschmst(Fltschmst fltschmst) {
		this.fltschmst = fltschmst;
	}

}