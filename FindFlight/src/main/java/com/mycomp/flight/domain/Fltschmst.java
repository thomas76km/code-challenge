package com.mycomp.flight.domain;

import java.io.Serializable;
import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the fltschmst database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Fltschmst.findAll", query="SELECT f FROM Fltschmst f"),
	@NamedQuery(name="Fltschmst.findFlightByOrgArpAndDstArp", query="SELECT fltmst FROM Fltschmst fltmst where fltmmst.orgArp=:orgArp and fltmst.dstArp=:dstArp"
			+ " and orgDepTimGmt > SYSDATE() - 1")
})
public class Fltschmst implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="FLT_ID")
	private int fltId;

	@Column(name="DST_ARP")
	private String dstArp;

	@Column(name="DST_ARR_TIM_GMT")
	private Timestamp dstArrTimGmt;

	@Column(name="ORG_ARP")
	private String orgArp;

	@Column(name="ORG_DEP_TIM_GMT")
	private Timestamp orgDepTimGmt;

	//bi-directional many-to-one association to Fltcondtl
	@OneToMany(mappedBy="fltschmst")
	private List<Fltcondtl> fltcondtls;

	public Fltschmst() {
	}

	public int getFltId() {
		return this.fltId;
	}

	public void setFltId(int fltId) {
		this.fltId = fltId;
	}

	public String getDstArp() {
		return this.dstArp;
	}

	public void setDstArp(String dstArp) {
		this.dstArp = dstArp;
	}

	public Timestamp getDstArrTimGmt() {
		return this.dstArrTimGmt;
	}

	public void setDstArrTimGmt(Timestamp dstArrTimGmt) {
		this.dstArrTimGmt = dstArrTimGmt;
	}

	public String getOrgArp() {
		return this.orgArp;
	}

	public void setOrgArp(String orgArp) {
		this.orgArp = orgArp;
	}

	public Timestamp getOrgDepTimGmt() {
		return this.orgDepTimGmt;
	}

	public void setOrgDepTimGmt(Timestamp orgDepTimGmt) {
		this.orgDepTimGmt = orgDepTimGmt;
	}

	public List<Fltcondtl> getFltcondtls() {
		return this.fltcondtls;
	}

	public void setFltcondtls(List<Fltcondtl> fltcondtls) {
		this.fltcondtls = fltcondtls;
	}

	public Fltcondtl addFltcondtl(Fltcondtl fltcondtl) {
		getFltcondtls().add(fltcondtl);
		fltcondtl.setFltschmst(this);

		return fltcondtl;
	}

	public Fltcondtl removeFltcondtl(Fltcondtl fltcondtl) {
		getFltcondtls().remove(fltcondtl);
		fltcondtl.setFltschmst(null);

		return fltcondtl;
	}

}