package com.mycomp.flight.service.dto;

import java.io.Serializable;
import java.sql.Timestamp;


public class FltcondtlDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private int fltcondtlId;

	private String conArp;

	private Timestamp depTim;

	public int getFltcondtlId() {
		return fltcondtlId;
	}

	public void setFltcondtlId(int fltcondtlId) {
		this.fltcondtlId = fltcondtlId;
	}

	public String getConArp() {
		return conArp;
	}

	public void setConArp(String conArp) {
		this.conArp = conArp;
	}

	public Timestamp getDepTim() {
		return depTim;
	}

	public void setDepTim(Timestamp depTim) {
		this.depTim = depTim;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((conArp == null) ? 0 : conArp.hashCode());
		result = prime * result + ((depTim == null) ? 0 : depTim.hashCode());
		result = prime * result + fltcondtlId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FltcondtlDTO other = (FltcondtlDTO) obj;
		if (conArp == null) {
			if (other.conArp != null)
				return false;
		} else if (!conArp.equals(other.conArp))
			return false;
		if (depTim == null) {
			if (other.depTim != null)
				return false;
		} else if (!depTim.equals(other.depTim))
			return false;
		if (fltcondtlId != other.fltcondtlId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FltcondtlDTO [fltcondtlId=" + fltcondtlId + ", conArp=" + conArp + ", depTim=" + depTim + "]";
	}
	
	

}
