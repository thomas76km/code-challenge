package com.mycomp.flight.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycomp.flight.FlightDTO;
import com.mycomp.flight.domain.Fltschmst;
import com.mycomp.flight.repository.FlightRepository;
import com.mycomp.flight.service.FlightService;
import com.mycomp.flight.service.dto.FltcondtlDTO;
import com.mycomp.flight.service.dto.FltschmstDTO;

@Service
@Transactional(rollbackFor = Exception.class)
public class FlightServiceImpl implements FlightService{
	
	@Autowired
	private FlightRepository flightRepository;

	public FltschmstDTO findFlightRecord(FlightDTO flightDTO) {
		
		FltschmstDTO fltschmstDTO = new FltschmstDTO();
		
		List <FltcondtlDTO> connectionArpList= new ArrayList();
		if (flightDTO != null)
		{
			Fltschmst fltschmst = flightRepository.findFlightByOrgArpAndDstArp(flightDTO.getOrgArp(), flightDTO.getDstArp());
			
			if (fltschmst != null)
			{
				fltschmstDTO.setDstArp(fltschmst.getDstArp());
				fltschmstDTO.setDstArrTimGmt(fltschmst.getDstArrTimGmt());
				fltschmstDTO.setFltId(fltschmst.getFltId());
				fltschmstDTO.setOrgArp(fltschmst.getOrgArp());
				fltschmstDTO.setOrgDepTimGmt(fltschmst.getOrgDepTimGmt());
				if (!fltschmst.getFltcondtls().isEmpty())
				{
					fltschmst.getFltcondtls().forEach(connectionArp-> {
						FltcondtlDTO fltcondtlDTO = new FltcondtlDTO();
						fltcondtlDTO.setConArp(connectionArp.getConArp());
						fltcondtlDTO.setDepTim(connectionArp.getDepTim());
						fltcondtlDTO.setFltcondtlId(connectionArp.getFltcondtlId());
						connectionArpList.add(fltcondtlDTO);
					});
				}
				fltschmstDTO.setFltcondtlsDTO(connectionArpList);
			}
		}
		return fltschmstDTO;
	}

}
