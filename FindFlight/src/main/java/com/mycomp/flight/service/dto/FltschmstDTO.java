package com.mycomp.flight.service.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import com.mycomp.flight.FlightDTO;
import com.mycomp.flight.domain.Fltcondtl;

public class FltschmstDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private int fltId;

	private String dstArp;

	private Timestamp dstArrTimGmt;

	private String orgArp;

	private Timestamp orgDepTimGmt;
	
	private List<FltcondtlDTO> fltcondtlsDTO; 

	public int getFltId() {
		return fltId;
	}

	public void setFltId(int fltId) {
		this.fltId = fltId;
	}

	public String getDstArp() {
		return dstArp;
	}

	public void setDstArp(String dstArp) {
		this.dstArp = dstArp;
	}

	public Timestamp getDstArrTimGmt() {
		return dstArrTimGmt;
	}

	public void setDstArrTimGmt(Timestamp dstArrTimGmt) {
		this.dstArrTimGmt = dstArrTimGmt;
	}

	public String getOrgArp() {
		return orgArp;
	}

	public void setOrgArp(String orgArp) {
		this.orgArp = orgArp;
	}

	public Timestamp getOrgDepTimGmt() {
		return orgDepTimGmt;
	}

	public void setOrgDepTimGmt(Timestamp orgDepTimGmt) {
		this.orgDepTimGmt = orgDepTimGmt;
	}
	
	
	public List<FltcondtlDTO> getFltcondtlsDTO() {
		return fltcondtlsDTO;
	}

	public void setFltcondtlsDTO(List<FltcondtlDTO> fltcondtlsDTO) {
		this.fltcondtlsDTO = fltcondtlsDTO;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dstArp == null) ? 0 : dstArp.hashCode());
		result = prime * result + ((dstArrTimGmt == null) ? 0 : dstArrTimGmt.hashCode());
		result = prime * result + fltId;
		result = prime * result + ((fltcondtlsDTO == null) ? 0 : fltcondtlsDTO.hashCode());
		result = prime * result + ((orgArp == null) ? 0 : orgArp.hashCode());
		result = prime * result + ((orgDepTimGmt == null) ? 0 : orgDepTimGmt.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FltschmstDTO other = (FltschmstDTO) obj;
		if (dstArp == null) {
			if (other.dstArp != null)
				return false;
		} else if (!dstArp.equals(other.dstArp))
			return false;
		if (dstArrTimGmt == null) {
			if (other.dstArrTimGmt != null)
				return false;
		} else if (!dstArrTimGmt.equals(other.dstArrTimGmt))
			return false;
		if (fltId != other.fltId)
			return false;
		if (fltcondtlsDTO == null) {
			if (other.fltcondtlsDTO != null)
				return false;
		} else if (!fltcondtlsDTO.equals(other.fltcondtlsDTO))
			return false;
		if (orgArp == null) {
			if (other.orgArp != null)
				return false;
		} else if (!orgArp.equals(other.orgArp))
			return false;
		if (orgDepTimGmt == null) {
			if (other.orgDepTimGmt != null)
				return false;
		} else if (!orgDepTimGmt.equals(other.orgDepTimGmt))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FltschmstDTO [fltId=" + fltId + ", dstArp=" + dstArp + ", dstArrTimGmt=" + dstArrTimGmt + ", orgArp="
				+ orgArp + ", orgDepTimGmt=" + orgDepTimGmt + ", fltcondtlsDTO=" + fltcondtlsDTO + "]";
	}

	

}
