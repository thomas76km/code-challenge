package com.mycomp.flight.service;

import com.mycomp.flight.FlightDTO;
import com.mycomp.flight.service.dto.FltschmstDTO;

public interface FlightService {

	public FltschmstDTO findFlightRecord(FlightDTO flightDTO);
}
