package com.mycomp.flight;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mycomp.flight.service.FlightService;
import com.mycomp.flight.service.dto.FltschmstDTO;

@RestController
public class FlightController {
	
	@Autowired
	private FlightService flightService;
	
	@PostMapping(value = "/findflight")
	public FltschmstDTO findFlight(@Valid @RequestBody FlightDTO flightDTO) {
		
		return flightService.findFlightRecord(flightDTO);
	}

}
